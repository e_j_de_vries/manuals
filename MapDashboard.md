# MapDashboard

## Installation

After downloading the data field synchronise the Edge with Garmin Connect Mobile (GCM) or Garmin Express (GE)

On the main screen go to settings:

![main screen](images/common/install_01_settings.png)

Select "Activity profiles":

![settings](images/common/install_02_settings.png)

Select the profile to use with MapDashboard

![profiles](images/common/install_03_settings.png)

Go to "Data Screens":

![profiles](images/common/install_04_settings.png)

Select "Map":

![profiles](images/mapdashboard/install_05_settings.png)

Select "Layout and fields":

![profiles](images/mapdashboard/install_06_settings.png)

Set the number of fields to "1" and press the field in the bottom of the screen:

![profiles](images/mapdashboard/install_07_settings.png)

Scroll to and select category "Connect IQ":

![profiles](images/mapdashboard/install_08_settings.png)

Select "MapDashboard" and select back:

![profiles](images/mapdashboard/install_09_settings.png)

Select back once more to return to the Map screen.

![profiles](images/mapdashboard/install_10_settings.png)



