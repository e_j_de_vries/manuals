# MapDashboardMS (Multi Sport)

## Installation

After downloading the data field synchronise the Edge with Garmin Connect Mobile (GCM) or Garmin Express (GE)

On the main screen go to settings:

![main screen](images/common/install_01_settings.png)

Select "Activity profiles":

![settings](images/common/install_02_settings.png)

Select the profile to use with MapDashboard

![profiles](images/common/install_03_settings.png)

Go to "Data Screens":

![Data screen](images/common/install_04_settings.png)

Select "Map":

![profiles](images/mapdashboard/install_05_settings.png)

Select "Layout and fields":

![profiles](images/mapdashboard/install_06_settings.png)

Set the number of fields to "1" and press the field in the bottom of the screen:

![profiles](images/mapdashboard/install_07_settings.png)

Scroll to and select category "Connect IQ":

![profiles](images/mapdashboard/install_08_settings.png)

Select "MapDashboardMS"

![profiles](images/mapdashboardms/install_09_settings.png)

The system asks to "Edit data field settings"

![profiles](images/mapdashboardms/install_01_appsettings.png)

Choose "yes" to go to the settings screen.
Tap the three bars in the lower right corner on the Edge 1030, the three dots in the top right corner on the Edge 1040 or hold the the top right button on the Edge 530 to go to the settings screen.

![profiles](images/mapdashboardms/install_02_appsettings.png)

The settings are shown for the current selected/active profile on the Edge. This can be a different profile than the profile that you have been adding this field to.

Enable "Hide headers" to hide the headers/labels while riding, this allows the use of a larger font for the field values.
Configure the six fields by clicking on them. Default settings are shown as "-" in the subtext.

![profiles](images/mapdashboardms/install_03_appsettings.png)

Clicking on a field leads to the next screen. Choose a field value.
Select a field value by clicking on it and return to the previous screen.

![profiles](images/mapdashboardms/install_04_appsettings.png)

After configuration click "back" to return to the Map Screen, the end result should look like this:

![profiles](images/mapdashboardms/install_finished.png)

## Configuration

Swipe down from the top to show the configuration screen:

![Configuration](images/mapdashboardms/config_01.png)

Swipe to the "Controls" screen and select "Connect IQ Data Fields":

![Controls](images/mapdashboardms/config_02.png)

Select "MapdashboardMS":

![MapdashboardMS](images/mapdashboardms/config_03.png)

Tap the three bars in the lower right corner on the Edge 1030, the three dots in the top right corner on the Edge 1040 or hold the the top right button on the Edge 530 to go to the settings screen.

![profiles](images/mapdashboardms/install_02_appsettings.png)

Enable "Hide headers" to hide the headers/labels while riding, this allows the use of a larger font for the field values.
Configure the six fields by clicking on them.

![profiles](images/mapdashboardms/install_03_appsettings.png)

Clicking on a field leads to the next screen. Choose a field value.
Select a field value by clicking on it and return to the previous screen.

![profiles](images/mapdashboardms/install_04_appsettings.png)


