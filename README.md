# README #

Manuals for Garmin apps of Garmin developer [Erik.de.Vries](https://apps.garmin.com/en-US/developer/8fc4b646-dd53-44ec-a4d8-e0db13cdbb13/apps)

### Apps ###

* [EdgeDashboard](EdgeDashboard.md)
* [MapDashboard](MapDashboard.md)
* [MapDashboard4](MapDashboard4.md)
* [MapDashboardMS](MapDashboardMS.md)


### Contact ###

* e.j.de.vries at gmail.com