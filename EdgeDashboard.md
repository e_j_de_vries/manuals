# EdgeDashboard

## Installation

After downloading the data field synchronise the Edge with Garmin Connect Mobile (GCM) or Garmin Express (GE)

On the main screen go to settings:

![main screen](images/common/install_01_settings.png)

Select "Activity profiles":

![settings](images/common/install_02_settings.png)

Select the profile to use with EdgeDashboard

![profiles](images/common/install_03_settings.png)

Go to "Data Screens":

![profiles](images/common/install_04_settings.png)

Select "Add new":

![profiles](images/edgedashboard/datascreens_add_new.png)

Select type "Data Screen":

![profiles](images/edgedashboard/datascreens_type.png)

Select category "Connect IQ":

![profiles](images/edgedashboard/datafields_types.png)

Select "EdgeDashboard":

![profiles](images/edgedashboard/datafields_edgedashboard.png)

Select back

![profiles](images/edgedashboard/datafields_edgedashboard_back.png)

Confirm the selection of EdgeDashboard

![profiles](images/edgedashboard/datafields_edgedashboard_screen.png)

Move the screen to the desired location:

![profiles](images/edgedashboard/datascreen_up_down.png)



